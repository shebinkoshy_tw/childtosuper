//
//  ChildToSuper.swift
//  BridgeManager
//
//  Created by Shebin Koshy on 06/05/20.
//  Copyright © 2020 Shebin Koshy. All rights reserved.
//

import UIKit

@objc public protocol ChildToSuperDelegate {
    
    @objc func childToSuper(action:String, arguments:[String:Any]?, completion:((Any?,Bool) -> Swift.Void)?)
    
}

public class ChildToSuper: NSObject {
    
    @objc public static let shared = ChildToSuper()
    @objc public var delegate: ChildToSuperDelegate?//One of the Super App Class Instance will be assigned
    
    private override init() {
        super.init()
    }
    
    @objc public func invokeSuper(action:String, arguments:[String:Any]?, completion:((Any?,Bool) -> Swift.Void)?) {
        delegate?.childToSuper(action: action, arguments: arguments, completion: completion)
    }

}

