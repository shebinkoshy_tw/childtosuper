Pod::Spec.new do |s|
  s.name         = "ChildToSuper"
  s.version      = "1.0.1"
  s.summary      = "Sample child app"
  s.description  = "The Sample child app"
  s.homepage     = "https://gitlab.com/shebinkoshy_tw/childtosuper.git"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "" => "Shebin Koshy" }
  s.platform     = :ios
  s.source       = { :git => "https://gitlab.com/shebinkoshy_tw/childtosuper.git", :tag => '1.0.1' }
  s.ios.deployment_target = "9.0"
  s.source_files = "ChildToSuper/ChildToSuper.swift"
end